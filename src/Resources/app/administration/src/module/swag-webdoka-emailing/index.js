import './page/swag-webdoka-emailing-list';
import enGB from './snippet/en-GB.json';
import deDE from './snippet/de-DE.json';

Shopware.Module.register('swag-webdoka-emailing', {
    type: 'plugin',
    name: 'Webdoka emailing',
    color: '#ffffff',
    icon: 'default-package-open',
    title: 'swag-webdoka-emailing.general.mainMenuItemGeneral',
    description: 'Manager email newsletters',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'swag-webdoka-emailing-list',
            path: 'list'
        },
        //detail:{}
        //create:{}
    },

    navigation: [{
        label: 'swag-webdoka-emailing.general.mainMenuItemGeneral',
        color: '#ffffff',
        path: 'swag.webdoka.emailing.list',
        icon: 'default-package-open',
        //parent: 'sw-marketing',
        position: 100
    }]

    //settingsItem: [{
    //    group: 'plugin',
    //    to: 'swag-webdoka-emailing-list',
    //    icon: 'default-object-rocket',
    //    name: 'swag-webdoka-emailing.general.mainMenuItemGeneral'
    //}]
});
