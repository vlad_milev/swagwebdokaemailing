<?php declare(strict_types=1);

namespace SwagWebdokaEmailing\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1666777649 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1666777649;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
            CREATE TABLE IF NOT EXISTS `webdoka_emailing` (
                `id` BINARY(16) NOT NULL,
                `name` VARCHAR(255) COLLATE utf8mb4_unicode_ci,
                `senddt` DATETIME(3),
                `letter` LONGTEXT COLLATE utf8mb4_unicode_ci,
                `active` TINYINT(1) NULL DEFAULT '0',
                `cuser` INT NULL DEFAULT '0',
                `csend` INT NULL DEFAULT '0',
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3),
                PRIMARY KEY (`id`)
            )
            ENGINE = InnoDB
            DEFAULT CHARSET = utf8mb4
            COLLATE = utf8mb4_unicode_ci;
            SQL;
        $connection->executeStatement($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
