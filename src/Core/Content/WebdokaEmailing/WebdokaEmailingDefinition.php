<?php declare(strict_types=1);

namespace SwagWebdokaEmailing\Core\Content\WebdokaEmailing;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;

class WebdokaEmailingDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'webdoka_emailing';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return WebdokaEmailingEntity::class;
    }

    public function getCollectionClass(): string
    {
        return WebdokaEmailingCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        /*
         * IdField id
         * StringField name
         * DateTimeField senddt
         * StringField letter
         * BoolField active
         * IntField cuser
         * IntField csend
         */
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            (new StringField('name', 'name'))->addFlags(new Required()),
            (new DateTimeField('senddt', 'senddt'))->addFlags(new Required()),
            (new StringField('letter', 'letter'))->addFlags(new Required()),
            (new BoolField('active', 'active')),
            (new IntField('cuser', 'cuser')),
            (new IntField('csend', 'csend'))
        ]);
    }
}
