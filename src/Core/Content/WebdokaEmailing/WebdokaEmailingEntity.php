<?php declare(strict_types=1);

namespace SwagWebdokaEmailing\Core\Content\WebdokaEmailing;

use pq\DateTime;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class WebdokaEmailingEntity extends Entity
{
    use EntityIdTrait;

    protected ?string $name;

    protected ?DateTime $senddt;

    protected ?string $letter;

    protected bool $active;

    protected int $cuser;

    protected int $csend;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getLetter(): ?string
    {
        return $this->letter;
    }

    public function setLetter(?string $letter): void
    {
        $this->letter = $letter;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return DateTime|null
     */
    public function getSenddt(): ?DateTime
    {
        return $this->senddt;
    }

    /**
     * @param DateTime|null $senddt
     */
    public function setSenddt(?DateTime $senddt): void
    {
        $this->senddt = $senddt;
    }

    /**
     * @return int
     */
    public function getCuser(): int
    {
        return $this->cuser;
    }

    /**
     * @param int $cuser
     */
    public function setCuser(int $cuser): void
    {
        $this->cuser = $cuser;
    }

    /**
     * @return int
     */
    public function getCsend(): int
    {
        return $this->csend;
    }

    /**
     * @param int $csend
     */
    public function setCsend(int $csend): void
    {
        $this->csend = $csend;
    }

}
