<?php declare(strict_types=1);

namespace SwagWebdokaEmailing\Core\Content\WebdokaEmailing;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void               add(ExampleEntity $entity)
 * @method void               set(string $key, ExampleEntity $entity)
 * @method WebdokaEmailingEntity[]    getIterator()
 * @method WebdokaEmailingEntity[]    getElements()
 * @method WebdokaEmailingEntity|null get(string $key)
 * @method WebdokaEmailingEntity|null first()
 * @method WebdokaEmailingEntity|null last()
 */
class WebdokaEmailingCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return WebdokaEmailingEntity::class;
    }
}
